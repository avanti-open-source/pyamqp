import pytest
import pika
import os
from pyamqp.rabbit.dispatcher import Dispatcher

# ----- Type Hints definitions ------
ChannelConnection = pika.adapters.blocking_connection.BlockingChannel


# -----------TEST VARIABLES-----------------
# Tuples containing host, username, password
# and other parameters config.
SERVERS = [
    ('172.31.8.123', os.getenv("RABBIT_USER"), os.getenv("RABBIT_PWD"))
]
# Tuples containing exchanges, exchange_type, auto_delete
EXCHANGES = [
    ('pyamqp_test', 'topic', True),
    ('test_pyamqp', 'direct', True),
    ('TEST_PY',  'direct', True)
]
# Tuples containing exchange, dict message, routing key and quantity
MESSAGES = [
    ('pyamqp_test', {'saludo': 'hello world!'}, 'TOPIC.TEST', 10),
    ('test_pyamqp', {'numero': 83182366}, 'DIRECT.TEST', 1000),
    ('TEST_PY', {'numero': 83182366}, 'DIRECT.TEST', 10000)
]

# ----------------OBJECTS-----------------------

# @pytest.fixture returns an object in order
# to be used in the "test_" functions as a parameter


@pytest.fixture
def connected_instance():
    host, user, password = SERVERS[0]
    return Dispatcher(host=host, username=user, password=password)


@pytest.fixture
def exchanges_declared_instance(connected_instance):
    """:return: Dispatcher instance connected to pyamqp_test exchange"""
    exchange, exchange_type, auto_delete = ('pyamqp_test', 'topic', True)
    connected_instance.connect_exchanges(
        exchange, exchange_type=exchange_type, auto_delete=auto_delete)
    return connected_instance


# ------------ Test functions ------------------

@pytest.mark.parametrize('server', SERVERS)
def test_class_initialization(server: tuple):
    host, user, password = server
    instance = Dispatcher(host=host, username=user, password=password)

    # If the channel connection is successful
    # the type of the connection will be BlockingChannel
    assert type(instance.channel_connection) == ChannelConnection


@pytest.mark.parametrize("exchange, exchange_type, auto_delete", EXCHANGES)
def test_connect_exchanges(connected_instance: Dispatcher,
                           exchange: str,
                           exchange_type: str,
                           auto_delete: bool):
    connected_instance.connect_exchanges(
        exchange, exchange_type=exchange_type, auto_delete=auto_delete)
    # Checks if the declaration of exchanges is successful
    assert exchange in connected_instance._exchanges_used


@pytest.mark.parametrize("exchange, message, routing_key, quantity", MESSAGES)
def test_send_messages(exchanges_declared_instance: Dispatcher,
                       exchange,
                       message,
                       routing_key,
                       quantity):
    # If the send_message function doesn't throw
    # any errors this test will pass
    for number in range(quantity):
        exchanges_declared_instance.send_message(
            message=message, key=routing_key, exchange=exchange)


def test_reconnection(connected_instance):
    connected_instance.reconnect()
    assert type(connected_instance.channel_connection) == ChannelConnection

