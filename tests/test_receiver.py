from pyamqp.rabbit.receiver import Receiver

import time
import os
import pytest
import pika
from typing import List, Union

# ----- Type Hints definitions ------
ChannelConnection = pika.adapters.blocking_connection.BlockingChannel

# --------- TEST VARIABLES ----------
# Tuples containing host, username, password
# and other parameters config.
SERVERS = [
    ('172.31.8.123', os.getenv("RABBIT_USER"), os.getenv("RABBIT_PWD"))
]
# Tuples containing queue_name, exchanges, routing_keys, auto_delete
# exchange_is_durable, exchange_type, exchange_auto_delete
QUEUES = [
    ('pyamqp_test', 'data', ['A', 'B'], True, True),
    ('pyamqp_test_2', 'data', ['A', 'C'], False, False),
    ('pyamqp_test_3', 'data', ['C', 'B'], True, False),
]

# ------------OBJECTS--------------
# @pytest.fixture returns an object in order
# to be used in the "test_" functions as a parameter


@pytest.fixture
def connected_instance():
    host, user, password = SERVERS[0]
    return Receiver(host=host, username=user, password=password)


@pytest.fixture
def queues_declared_instance(connected_instance):
    """:return: Receiver instance connected to pyamqp_test exchange"""
    connected_instance.connect_queue(*QUEUES[0])
    yield connected_instance


# -------- Test functions -----------
@pytest.mark.parametrize('server', SERVERS)
def test_class_initialization(server: tuple):
    host, user, password = server
    instance = Receiver(host=host, username=user, password=password, threaded=True)
    # If the channel connection is successful
    # the type of the connection will be BlockingChannel
    assert type(instance.channel_connection) == ChannelConnection


def test_class_reconnection(queues_declared_instance: Receiver):
    def callback(message, details):
        assert isinstance(message, dict) and isinstance(details, dict)
    queues_declared_instance.consume(callback)
    queues_declared_instance.consumer_reconnect()
    assert type(queues_declared_instance.channel_connection) == ChannelConnection
    queues_declared_instance.close()


@pytest.mark.parametrize(
    "queue_name, exchange, routing_keys, durability, auto_delete", QUEUES)
def test_connect_queues(connected_instance: Receiver,
                        queue_name: str,
                        exchange: str,
                        routing_keys: Union[str, List[str]],
                        durability: bool,
                        auto_delete: bool):
    connected_instance.connect_queue(
        queue_name=queue_name,
        exchange=exchange,
        routing_keys=routing_keys,
        is_durable=durability,
        auto_delete=auto_delete
    )
    assert queue_name == connected_instance._last_queue_name


def test_receive_messages(queues_declared_instance: Receiver):
    # Asserts if the message and details variable makes sense.
    # Will throw error if they are not dict instance or
    # if consume fails
    def callback(message, details):
        print(message)
        assert isinstance(message, dict) and isinstance(details, dict)

    queues_declared_instance.consume(callback)
    # This sleep is needed in order to assert message receiving
    time.sleep(5)
    queues_declared_instance.close()


def test_reconnection(connected_instance):
    connected_instance.reconnect()
    assert type(connected_instance.channel_connection) == ChannelConnection


