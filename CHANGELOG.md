# Changelog
All notable changes will be specified in this file.

# Future updates
- Different types of connections (Twisted, Selected) for RabbitMQ 

## [0.0.4]
### Added
- Receiver class unit tests.

## [0.0.3]
### Added
- Receiver class.

## [0.0.2]
### Added
- Dispatcher class unit tests.

## [0.0.1] - 2019-02-05
### Added
- Tests directory and package.
- Dispatcher class.

