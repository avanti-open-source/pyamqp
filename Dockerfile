FROM python:3.7-buster

ADD requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

ENV RABBIT_USER=$RABBIT_USER
ENV RABBIT_PWD=$RABBIT_PWD
# Copying the repository into the image
COPY . .

